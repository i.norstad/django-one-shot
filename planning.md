# One Shot Planning

These are some markdown formatting you can use to plan. You can preview your markdown in VS Code with the command "markdown preview", commonly `Command + Shift + V` (`Ctrl + Shift + V` on Windows) while you have this file open

Edit this and make it your own. Alternatively, link your notion here:

## My Notion / weblink


Helpful commands
This section is here to give you a reference for common commands that you may need to use during this (and other) projects.

# Create a new virtual environment
# Make sure you do this in a new terminal window or
# that you have NOT ALREADY activated a virtual
# environment
python -m venv .venv


# Activate a virtual environment
source ./.venv/bin/activate  # macOS

# Deactivating your virtual environment
# If you are in an active virtual environment,
# then you can deactivate it with this
# command
deactivate


# Update pip
python -m pip install --upgrade pip


# Install dependencies from requirements.txt
pip install -r requirements.txt


# Create a brand new requirements.txt file from
# the installed pip packages
pip freeze > requirements.txt


# Create a new Django project in the current
# directory, normally the directory that contains
# your .venv directory. DO NOT FORGET THE DOT!
django-admin startproject «name» .


# Create a new Django app
python manage.py startapp «name»


# Run your development server
python manage.py runserver


# Test your application
python manage.py test


# Make migrations after creating or changing
# a model class
python manage.py makemigrations


# Apply the migrations to your database after
# making them
python manage.py migrate
Helpful reminders
Here are some helpful reminders about how to use Django:

After creating a new Django app, make sure you add it to the top of the Django project's INSTALLED_APPS list in settings.py.

INSTALLED_APPS = [
  "app_name.apps.AppNameConfig",  # replace with your
  ...                             # app's name
]
After creating at least one path entry in the urlpatterns list of a new Django app's urls.py (you have to create this file yourself), make sure to include it in the Django project's urls.py.

path("prefix/", include("app_name.urls")) # Replace with
                                          # your app's name
In your templates, use the Django url tag to generate the correct values for href values for anchor tags.

<a href="{% url 'path_name' %}">
The path_name value comes from the path registration.

                       # This is the path name used
                       # in the url tag, reverse,
                       # reverse_lazy, and settings
                       # |
path("some/path/", view, name="path_name")
If your path registration has an <int:id> in the path, like this one:

         # This <int:id> means that you need to
         # have an id for the url tag
         # |
path("some/<int:id>/", view, name="path_name")
then that's when you need to provide an argument to the url tag.

<a href="{% url 'path_name' item.id %}">
Models
Models should always inherit from models.Model.

Here's a typical model example:

class MyModel(models.Model):
  # This defines the fields of your model
  name = models.CharField(max_length=100)

  # This tells Django how to convert our model into a string
  # when we print() it, or when the admin displays it.
  def __str__(self):
    return self.name
QuerySets
These are the primary methods for getting objects from the database:

# Get all the objects
model_instance_list = ModelName.objects.all()
# Get the first object
model_instance = ModelName.objects.first()
# Get a single object by one of it's attributes like:
# By primary key
model_instance = ModelName.objects.get(id=1)
# By some other field
model_instance = ModelName.objects.get(some_field="Some field value")
# Filter is like get, but it gets all the objects that match
model_instance_list = ModelName.objects.filter(some_field="Some field value")
Registering a model in the admin
When registering a model, make sure you do two things:

Create a class that inherits from admin.ModelAdmin
Register the class along with your model using the @admin.register(model) decorator.
Helpful view and template patterns
Here are some general templates that you can use for class views. They have the fields and methods for the default functionality of the views.

Of course, replace the generic parts like ModelName with the model name you're interested in using.

For a given model name, these views look for templates in your Django app under templates/model_names where "model_names" is the plural underscored version of the model's name. (You can name yours whatever you want.)

Filtering objects
When you need to filter objects for the HTML template, you use the filter method for that.

For function views, you just use that filter when you're creating your context.

def my_view(request):
    # other code here to do other stuff
    context = {
        "whatever key name": MyModel.objects.filter(property=value),
        # other context variables, if needed
    }
    return render(request, "whatever/template.html", context)


List views
def show_model_name(request):
  model_list = ModelName.objects.all()
  context = {
    "model_list": model_list
  }
  return render(request, "model_names/list.html", context)
Detail views
def show_model_name(request, id):
  model_instance = ModelName.objects.get(id=id)
  context = {
    "model_instance": model_instance
  }
  return render(request, "model_names/detail.html", context)



Create views
def create_model_name(request):
  if request.method == "POST":
    form = ModelForm(request.POST)
    if form.is_valid():
      # To redirect to the detail view of the model, use this:
      model_instance = form.save()
      return redirect("detail_url", id=model_instance.id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
  else:
    form = ModelForm()

  context = {
    "form": form
  }

  return render(request, "model_names/create.html", context)


Update views
def update_model_name(request, id):
  model_instance = ModelName.objects.get(id=id)
  if request.method == "POST":
    form = ModelForm(request.POST, instance=model_instance)
    if form.is_valid():
      # To redirect to the detail view of the model, use this:
      model_instance = form.save()
      return redirect("detail_url", id=model_instance.id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
  else:
    form = ModelForm(instance=model_instance)

  context = {
    "form": form
  }

  return render(request, "model_names/edit.html", context)
Delete views
def delete_model_name(request, id):
  model_instance = ModelName.objects.get(id=id)
  if request.method == "POST":
    model_instance.delete()
    return redirect("some_url")

  return render(request, "models_names/delete.html")


Templates with forms
Almost every template with a form will have some variation of the following code snippet with more HTML around it, or some added stuff in to make it prettier. But this is the general pattern:

<form method="post">
  {% csrf_token %}
  {{ form.as_p }}
  <button>Login</button>
</form>
