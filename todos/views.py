from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodolistForm, TodoItemForm


# Create your views here.


# create todo list
def todo_list_create(request):
    if request.method == "POST":
        form = TodolistForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodolistForm()
    context = {
        "form": form,
    }
    return render(request, "todo_lists/create.html", context)


# list view of todo_lists
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list,
    }
    return render(request, "todo_lists/list.html", context)


# show detail of lists, including tasks
def show_todo_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todo_lists/detail.html", context)


# update todo list
def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodolistForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodolistForm(instance=todo_list)
    context = {"todo_list": todo_list, "form": form}
    return render(request, "todo_lists/edit.html", context)


# delete todo list
def todo_list_delete(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")
    context = {
        "ist": list,
    }
    return render(request, "todo_lists/delete.html", context)


# create a new todo item
def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "items/create.html", context)


# update a todo item
def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", item.list.id)
    else:
        form = TodoItemForm(instance=item)
    context = {"item": item, "form": form}
    return render(request, "items/edit.html", context)
